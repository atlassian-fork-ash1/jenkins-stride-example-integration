const path = require('path')
let jsonPath = path.join(process.cwd(), process.argv.slice(2)[0])
let json = require(jsonPath)

function escapeDoubleQuotes(str) {
	return str.replace(/\\([\s\S])|(")/g,"\\$1$2"); // thanks @slevithan!
}

console.log('"'+escapeDoubleQuotes(JSON.stringify(json))+'"')